namespace MattEland.Shared.Text

module Cleaning =

    // Gets a sequence of tokens in an input
    let tokenize (text: string): string seq = text.Split [|' '|] |> Array.toSeq 

    /// Takes a character and moves it to uppercased format
    let toUpperInvariant (c: char) = 
       System.Char.ToUpperInvariant c

    /// Takes a string and applies standard transformations to it
    let standardize (text: string) =
        text.Trim()
        |> String.map toUpperInvariant
