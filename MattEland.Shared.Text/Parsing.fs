﻿namespace MattEland.Shared.Text

module Parsing =

  /// Interprets the input text via a Natural Language Processing library and returns the results
  let interpret text =
    printfn "Interpreting text: %s" text

    let sb = new System.Text.StringBuilder()

    // TODO: This is messy. I should partially apply a function here instead and extract out a method
    let formatString (token: string): unit = sb.AppendFormat("{0} ", token) |> ignore

    Cleaning.tokenize text |> Seq.iter formatString

    sb.ToString().Trim()