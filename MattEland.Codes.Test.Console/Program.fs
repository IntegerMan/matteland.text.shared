﻿open MattEland.Codes
open MattEland.Shared.Text

let processText = Parsing.interpret >> Cleaning.standardize
let processTextAndDisplayResult = processText >> printfn "Interpreted results: %s"

[<EntryPoint>]
let main argv =
    processTextAndDisplayResult "I'm Batman"
    0 
